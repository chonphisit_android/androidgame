package com.chonphisit.gamecalculrator2

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Button
import android.widget.TextView

class gamecalculrator2 : AppCompatActivity() {

    private val SECOND_ACTIVTY_REQUEST_CODE = 0
    private var sumcorrent = "0"
    private var sumincorrent = "0"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gamecalculrator2)
        val header = findViewById<TextView>(R.id.textheader)
        header.text = ("คิดเลขสนุก 1-10")

        val textcorrect = findViewById<TextView>(R.id.textcorrect)
        textcorrect.text = ("ถูก :")
        val textincorrect = findViewById<TextView>(R.id.textincorrect)
        textincorrect.text = ("ไม่ถูกต้อง :")

        val btnplus = findViewById<Button>(R.id.btnplus)
        val btnminus = findViewById<Button>(R.id.btnminus)
        val btnmultiply     = findViewById<Button>(R.id.btnmultiply)

        btnplus.setOnClickListener {
            val intent = Intent(gamecalculrator2@this, PlusActivity::class.java)
            startActivityForResult(intent, SECOND_ACTIVTY_REQUEST_CODE)
        }

        btnminus.setOnClickListener {
            val intent = Intent(gamecalculrator2@this, MinusActivity::class.java)
            startActivityForResult(intent, SECOND_ACTIVTY_REQUEST_CODE)
        }

        btnmultiply.setOnClickListener {
            val intent = Intent(gamecalculrator2@this, MultiplyActivity::class.java)
            startActivityForResult(intent, SECOND_ACTIVTY_REQUEST_CODE)
        }



    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == SECOND_ACTIVTY_REQUEST_CODE){
            if(resultCode == Activity.RESULT_OK){
                val returnAnscorrect = data!!.getStringExtra("sumcorrect")
                val returnAnsincorrect = data!!.getStringExtra("sumincorrect")
                val textcorrect = findViewById<TextView>(R.id.pcorrect)
                val textincorrect = findViewById<TextView>(R.id.pincorrect)
                sumcorrent = (sumcorrent.toString().toInt()+returnAnscorrect.toString().toInt()).toString()
                sumincorrent = (sumincorrent.toString().toInt()+returnAnsincorrect.toString().toInt()).toString()
                textcorrect.text = sumcorrent
                textincorrect.text = sumincorrent
            }
        }
    }
}