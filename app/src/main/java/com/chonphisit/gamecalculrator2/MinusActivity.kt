package com.chonphisit.gamecalculrator2

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_minus.*
import kotlinx.android.synthetic.main.activity_plus.*
import kotlinx.android.synthetic.main.activity_plus.textalert
import kotlin.random.Random

class MinusActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_minus)

        start()
    }

    private fun start() {
        val number1 = findViewById<TextView>(R.id.num1)
        val number2 = findViewById<TextView>(R.id.num2)
        Log.e("ERRor","TTTT")

        //random
        val randomnumber = Random.nextInt(0, 10)
        number1.text = randomnumber.toString()
        val randomnumber1 = Random.nextInt(0, 10)
        number2.text = randomnumber1.toString()

        val answer = (randomnumber - randomnumber1)
        val btnanswer1 = findViewById<Button>(R.id.btnanswer1)
        val btnanswer2 = findViewById<Button>(R.id.btnanswer2)
        val btnanswer3 = findViewById<Button>(R.id.btnanswer3)
        val choice = Random.nextInt(0, 4)

        if (choice == 0) {
            btnanswer1.text = answer.toString()
            btnanswer2.text = (answer + 1).toString()
            btnanswer3.text = (answer - 1).toString()
        } else if (choice == 1) {
            btnanswer1.text = (answer + 1).toString()
            btnanswer2.text = answer.toString()
            btnanswer3.text = (answer - 1).toString()
        } else {
            btnanswer1.text = (answer - 1).toString()
            btnanswer2.text = (answer + 1).toString()
            btnanswer3.text = answer.toString()
        }

        val pointcorrect = findViewById<TextView>(R.id.pointcorrect)
        val correct: Int = 0
        val pointincorrect = findViewById<TextView>(R.id.pointincorrect)
        val incorrect: Int = 0

        btnanswer1.setOnClickListener {
            if (btnanswer1.text.toString().toInt() == answer) {
                ColorCorrect(textalert)
                pointcorrect(correct)
                start()
            }else{
                ColorIncorrect(textalert)
                pointIncorrect(incorrect)
                start()
            }
        }
        btnanswer2.setOnClickListener {
            if (btnanswer2.text.toString().toInt() == answer) {
                ColorCorrect(textalert)
                pointcorrect(correct)
                start()
            }else{
                ColorIncorrect(textalert)
                pointIncorrect(incorrect)
                start()
            }
        }
        btnanswer3.setOnClickListener {
            if (btnanswer3.text.toString().toInt() == answer) {
                ColorCorrect(textalert)
                pointcorrect(correct)
                start()
            }else{
                ColorIncorrect(textalert)
                pointIncorrect(incorrect)
                start()
            }
        }
    }
    private fun ColorIncorrect(textalert: TextView?) {
        textalert!!.text = ("Incorrect")
        textalert.setTextColor(Color.RED)
    }

    private fun pointIncorrect(incorrect: Int) {
        pointincorrectminus.text = (pointincorrectminus.text.toString().toInt()+1).toString()
        menufun()
    }

    private fun ColorCorrect(textalert: TextView?) {
        textalert!!.text = ("correct")
        textalert.setTextColor(Color.GREEN)
    }

    private fun pointcorrect(correct: Int) {
        pointcorrectminus.text = (pointcorrectminus.text.toString().toInt()+1).toString()
        menufun()
    }
    private fun menufun(){
        val intent = Intent()
        intent.putExtra("sumincorrect", pointincorrectminus.text.toString())
        intent.putExtra("sumcorrect", pointcorrectminus.text.toString())
        setResult(Activity.RESULT_OK, intent)
    }

}